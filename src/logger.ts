import * as log4js from 'log4js';

type LogLevel = 'error' | 'warn' | 'info' | 'verbose' | 'debug' | 'silly';


export function getLogger(name: string, level: LogLevel) {
  const logger = log4js.getLogger(name);
  logger.level = level;
  return logger;
}
