import * as Discord from 'discord.js';
import * as nconf from 'nconf';
import { getLogger } from './logger';

nconf.argv().env().file('config/config.json');

const token = require('../config/token.json');
const LOG = getLogger('framebot', nconf.get('bot').logLevel);

LOG.info('Creating new client');
const client = new Discord.Client();

client.on('ready', () => {
  LOG.info('Bot ready');
});

client.on('message', (message: any) => {
  LOG.info(`Got message: ${message}`);
  if (message.mentions.users.has(client.user.id)) {
    message.reply('you mentioned me!');
  }
});

client.login(token.value);

// vim: ts=2
